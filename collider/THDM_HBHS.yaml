##########################################################################
## GAMBIT configuration for running a diver scan of the THDM
## THDM Type II (with running)
##
## Includes all compatible likelihoods:
## Theoretical, experimental & flavor
##
## Requires backends:
## THDMC, SuperISO, HiggsBounds, HiggsSignals, HEPLike
##########################################################################

Parameters:
  StandardModel_SLHA2: !import ../../include/StandardModel_SLHA2_defaults.yaml

  THDMIIatQ:
      lambda_1:
          range: [-0.5, 12.5664]
          prior_type: flat
      lambda_2:
          range: [-0.5, 12.5664]
          prior_type: flat
      lambda_3:
          range: [-12.5664, 12.5664]
          prior_type: flat
      lambda_4:
          range: [-12.5664, 12.5664]
          prior_type: flat
      lambda_5:
          range: [-12.5664, 12.5664]
          prior_type: flat
      # --- Assuming a CP-conserving THDM model ---
      lambda_6:
          fixed_value: 0.0
      lambda_7:
          fixed_value: 0.0
      # -------------------------------------------
      m12_2:
          ranges: [-1e6, -1e4, 1e4, 1e7]
          prior_type: double_log_flat_join
      tanb:
          range: [0.1, 100]
          prior_type: log
      Qin:
          fixed_value: 91.1876 # = mZ
      QrunTo:
          fixed_value: 1000 # for comparison with other literature

##############################
# Printer setup
##############################

Printer:

  # Select printer to use via string tag
  printer: hdf5

  options:
    # name of output file
    output_file: "GAMBIT_THDMIIatQ_scan_output.hdf5"
    group: "/data"
    delete_file_on_restart: false


##############################
# Scanner setup
##############################

Scanner:
  use_scanner: de

  scanners:
    de:
      plugin: diver
      like: LogLike
      NP: 50000
      convthresh: 1e-5
      verbosity: 1

    random:
      plugin: random
      point_number: 10000
      like:  LogLike

      # No options to set, but still need the nodes at the moment...
      aux_printer_txt_options:
      aux_printer_stats_options:
      aux_printer_live_options:

ObsLikes:

  ###########################
  # Likelihoods
  ###########################

  - purpose:    LogLike
    capability: h0_loop_order_corrections
    function: check_h0_loop_order_corrections

  - purpose:    LogLike
    capability: THDM_scalar_loop_order_corrections
    function: check_THDM_scalar_loop_order_corrections

  - purpose:    LogLike
    capability: THDM_scalar_masses
    function: check_THDM_scalar_masses

  - purpose:    LogLike
    capability: perturbativity_likelihood_THDM

  - purpose:    LogLike
    capability: stability_likelihood_THDM

  - purpose:    LogLike
    capability: NLO_unitarity_likelihood_THDM

  # - purpose:    LogLike
  #   capability: oblique_parameters_likelihood_THDM

  # - purpose:    LogLike
  #   capability: b2sgamma_LogLikelihood

  # - capability: deltaMB_LL
  #   function: deltaMB_likelihood
  #   purpose: LogLike

  # - capability: deltaMBd_LL
  #   function: deltaMBd_likelihood
  #   purpose: LogLike

  # - capability: b2ll_LL
  #   function: b2ll_likelihood
  #   purpose: LogLike

  # - capability: SL_LL
  #   function: SL_likelihood
  #   purpose: LogLike

  # - purpose:    LogLike
  #   capability: LUV_LL

  ## -------------------------------------------

  # -------------------------
  # OFF - Currently not working in combo with HS

  # - purpose:    LogLike
  #   capability: B2mumu_LogLikelihood_LHCb

  # - purpose:    LogLike
  #   capability: B2mumu_LogLikelihood_CMS

  # - purpose:    LogLike
  #   capability: B2mumu_LogLikelihood_Atlas

  # -------------------------


  # -------------------------
  # OFF - Not using in final scan
   
  # - purpose:    LogLike
  #   capability: B2KstarmumuAng_LogLikelihood_Atlas

  # - purpose:    LogLike
  #   capability: B2KstarmumuAng_LogLikelihood_CMS

  # - purpose:    LogLike
  #   capability: B2KstarmumuAng_LogLikelihood_Belle

  # - purpose:    LogLike
  #   capability: B2KstarmumuBr_LogLikelihood_LHCb

  # - purpose:    LogLike
  #   capability: B2KmumuBr_LogLikelihood_LHCb

  # -------------------------


  # -------------------------
  # OFF - Currently not working at all

  # - purpose:    LogLike # 
  #   capability: B2KstarmumuAng_LogLikelihood_LHCb_2020

  # -------------------------


  # -------------------------
  # OFF - Need capability SuperIso_theory_covariance which is not on branch

  # - purpose: LogLike
  #   capability: RDRDstar_LogLikelihood

  # - purpose: LogLike
  #   capability: RKstar_LogLikelihood_LHCb

  # -------------------------

  ## -------------------------------------------

  - capability: LEP_Higgs_LogLike
    function: calc_HB_5_LEP_LogLike
    purpose: LogLike
    dependencies:
      - capability: HB_ModelParameters_neutral
        function: THDM_ModelParameters_effc
        module: ColliderBit
      - capability: HB_ModelParameters_charged
        function: THDM_ModelParameters_charged
        module: ColliderBit

  - capability: LHC_Higgs_LogLike
    function: calc_HS_2_LHC_LogLike
    purpose: LogLike
    dependencies:
      - capability: HB_ModelParameters_neutral
        function: THDM_ModelParameters_effc
        module: ColliderBit
      - capability: HB_ModelParameters_charged
        function: THDM_ModelParameters_charged
        module: ColliderBit

  #################
  # Observables
  #################

  # THDM parameters

  - capability: THDM_spectrum_map
    function: get_THDM_spectrum_as_map
    purpose: Observable

  # branching fractions

  #- capability: all_BFs
  #  purpose: Observable

  # vacuum meta-stability discriminant

  - capability: vacuum_global_minimum
    function: check_vacuum_global_minimum
    purpose: Observable  

  # angles

  - capability: sba
    function: obs_sba
    purpose: Observable

  - capability: cba
    function: obs_cba
    purpose: Observable

  - capability: ba
    function: obs_ba
    purpose: Observable

  - capability: vev
    function: obs_vev
    purpose: Observable

  # bsgamma

  #- capability: bsgamma
  #  function: SI_bsgamma
  #  purpose: Observable

  # meson mixing

  # - capability: DeltaMs
  #   function: SI_Delta_MBs
  #   purpose: Observable

  # - capability: DeltaMd
  #   function: SI_Delta_MBd
  #   purpose: Observable

    # RD

  # - capability: RD
  #   function: SI_RD
  #   purpose: Observable

  #   # RDstar

  # - capability: RDstar
  #   function: SI_RDstar
  #   purpose: Observable
    
  #   # BDmunu

  # - capability: BDmunu
  #   function: SI_BDmunu
  #   purpose: Observable

  #   # BDstarmunu

  # - capability: BDstarmunu
  #   function: SI_BDstarmunu
  #   purpose: Observable

  #   # Btaunu

  # - capability: Btaunu
  #   function: SI_Btaunu
  #   purpose: Observable

  #   # Dstaunu

  # - capability: Dstaunu
  #   function: SI_Dstaunu
  #   purpose: Observable

  #   # Dsmunu

  # - capability: Dsmunu
  #   function: SI_Dsmunu
  #   purpose: Observable

  #   # Dmunu

  # - capability: Dmunu
  #   function: SI_Dmunu
  #   purpose: Observable

  #   # BKstarmumu_11_25

  # - capability: BKstarmumu_11_25
  #   function: SI_BKstarmumu_11_25
  #   purpose: Observable

  #   # BKstarmumu_25_40

  # - capability: BKstarmumu_25_40
  #   function: SI_BKstarmumu_25_40
  #   purpose: Observable

  #   # BKstarmumu_40_60

  # - capability: BKstarmumu_40_60
  #   function: SI_BKstarmumu_40_60
  #   purpose: Observable

  #   # BKstarmumu_60_80

  # - capability: BKstarmumu_60_80
  #   function: SI_BKstarmumu_60_80
  #   purpose: Observable

  #   # BKstarmumu_15_17

  # - capability: BKstarmumu_15_17
  #   function: SI_BKstarmumu_15_17
  #   purpose: Observable

  #   # BKstarmumu_17_19

  # - capability: BKstarmumu_17_19
  #   function: SI_BKstarmumu_17_19
  #   purpose: Observable

    # Bsmumu_untag

  # - capability: Bsmumu_untag
  #   function: SI_Bsmumu_untag
  #   purpose: Observable

  #   # Bmumu

  # - capability: Bmumu
  #   function: SI_Bmumu
  #   purpose: Observable
     

Rules:

  ##############################
  # Likelihood/Observable Rules
  ##############################

  - capability: THDM_spectrum
    function: get_THDM_spectrum

  - capability: decay_rates
    function: all_decays
    module: DecayBit

  - capability: all_BFs
    function: get_decaytable_as_map
    options:
      printall: true
      
  - capability: THDM_scalar_masses
    function: check_THDM_scalar_masses
    options:
      maximum_scalar_mass: 3000

  - capability: perturbativity_likelihood_THDM
    function: get_perturbativity_likelihood_THDM
    options:
      check_all_scales: true
      simple_perturbativity: false

  - capability: stability_likelihood_THDM
    function: get_stability_likelihood_THDM
    options:
      check_all_scales: true

  - capability: NLO_unitarity_likelihood_THDM
    function: get_NLO_unitarity_likelihood_THDM
    options:
      check_correction_ratio: true
      wave_function_corrections: true
      gauge_corrections: true
      yukawa_corrections: true

  - capability: Reference_SM_Higgs_decay_rates
    function: Ref_SM_Higgs_decays_THDM

  - capability: Reference_SM_other_Higgs_decay_rates
    function: Ref_SM_other_Higgs_decays_THDM

  - capability: Reference_SM_A0_decay_rates
    function: Ref_SM_A0_decays_THDM

  - capability: t_decay_rates
    function: t_decays_THDM

  - capability: HB_ModelParameters_neutral
    function: THDM_ModelParameters_effc
    module: ColliderBit

  - capability: HB_ModelParameters_charged
    function: THDM_ModelParameters_charged
    module: ColliderBit

  - capability: Higgs_Couplings
    function: THDM_higgs_couplings_2HDMC
    module: SpecBit

  - capability: SuperIso_modelinfo
    function: SI_fill
  
  - capability: bsgamma
    function: SI_bsgamma

  - capability: DeltaMs
    function: SI_Delta_MBs

  - capability: Bsmumu_untag
    function: SI_Bsmumu_untag
    module: FlavBit

  # -------------------------
  # OFF - b2mumu likelihoods

  # - capability: prediction_B2mumu
  #   options:
  #     obs_list: [BRuntag_Bsmumu, BR_Bdmumu]

  # - capability: B2mumu_LogLikelihood_.*
  #   function: HEPLike_B2mumu_LogLikelihood_LHCb
  #   options:
  #     obs_list: [BRuntag_Bsmumu, BR_Bdmumu]

  # -------------------------


  # -------------------------
  # OFF - b2kstar likelihoods

  # - capability: prediction_B2KstarmumuAng_.*_Atlas
  #   options:
  #     obs_list: [FL, S3, S4, S5, S7, S8]
  
  # - capability: B2KstarmumuAng_LogLikelihood_Atlas
  #   options:
  #     obs_list: [FL, S3, S4, S5, S7, S8]

  # - capability: prediction_B2KstarmumuAng_.*_Belle
  #   options:
  #     obs_list: [P4prime, P5prime]

  # - capability: B2KstarmumuAng_LogLikelihood_Belle
  #   options:
  #     obs_list: [P4prime, P5prime]

  # - capability: prediction_B2KstarmumuAng_.*_LHCb
  #   options:
  #     obs_list: [FL, AFB, S3, S4, S5, S7, S8, S9]
  
  # - capability: B2KstarmumuAng_LogLikelihood_LHCb_2020
  #   function: HEPLike_B2KstarmumuAng_LogLikelihood_LHCb_2020
  #   options:
  #     obs_list: [FL, AFB, S3, S4, S5, S7, S8, S9]  

  # - capability: prediction_B2KstarmumuAng_.*_CMS
  #   options:
  #     obs_list: [P1, P5prime] 

  # - capability: B2KstarmumuAng_LogLikelihood_CMS
  #   options:
  #     obs_list: [P1, P5prime]

  # -------------------------


  # -------------------------
  # OFF - RK/RD star likelihoods

  # - capability: SuperIso_theory_covariance
  #   function: SI_theory_covariance

  # -------------------------


#########################
# Logging setup
#########################

Logger:

  redirection:
    [Debug] :  "Debug.log"
    [Default] :  "Default.log"
    [DecayBit] :  "DecayBit.log"
    [DarkBit] :  "DarkBit.log"
    [PrecisionBit] :  "PrecisionBit.log"
    [ColliderBit] :  "ColliderBit.log"
    [SpecBit] :  "SpecBit.log"
    [Dependency Resolver] :  "DepResolver.log"
    [Scanner]:  "Scanner.log"

##########################
# Name/Value Section
##########################

KeyValues:

  likelihood:
    model_invalid_for_lnlike_below: -1e8
    model_invalid_for_lnlike_below_alt: -5e7
    debug: false
    
  default_output_path: "${CCCSCRATCHDIR}/THDMIIatQ/collider/HBHS"
  debug: false
