#!/bin/bash 
#MSUB -r gambit_THDM              # Request name 
#MSUB -n 128                     # Total number of tasks to use 
#MSUB -T 86400                  # Elapsed time limit in seconds (24 hours max)
#MSUB -o stdo_%I.o              # Standard output. %I is the job id 
#MSUB -e stde_%I.e              # Error output. %I is the job id
#MSUB -A ra5528                 # Project space
#MSUB -q rome                   # Partition
#MSUB -m scratch,work           # file systems

set -x 
cd ${BRIDGE_MSUB_PWD}
gambit_dir=${BRIDGE_MSUB_PWD}/../../../
yaml_file=${gambit_dir}/yaml_files/THDM_JC/collider/THDM_HS_no_theory_tree.yaml
env_setup_file=${gambit_dir}/yaml_files/THDM_JC/scripts/env_setup.sh
# setup env
${env_setup_file}
# run gambit
ccc_mprun ${gambit_dir}/gambit -rf ${yaml_file} > /dev/null
