# login
module switch dfldatadir/ra5528
# start up
module purge
module load ccc/1.0 flavor/buildtarget/x86_64 feature/mkl/single_node flavor/buildmpi/openmpi/4.0 flavor/openmpi/standard feature/openmpi/net/auto .tuning/openmpi/4.0.2 hwloc/2.0.4 sharp/2.0 hcoll/4.4.2938 pmix/3.1.3 ucx/1.7.0 flavor/buildcompiler/intel/19 feature/mkl/lp64 feature/mkl/sequential mkl/19.0.5.281 licsrv/intel c/intel/19.0.5.281 c++/intel/19.0.5.281 fortran/intel/19.0.5.281 gnu/8.3.0 intel/19.0.5.281 mpi/openmpi/4.0.2 datadir/ra5528 dfldatadir/ra5528 python3/3.6.4 git/2.19.1 cmake/3.9.1 flavor/boost/standard boost/1.69.0 eigen/3.2.8 hdf5/1.8.20 gsl/2.1 root/6.18.04 yaml-cpp/0.6.2
export LIBRARY_PATH="$LIBRARY_PATH:/ccc/products/ifort-19.0.5.281/system/default/19.0.5.281/lib/intel64"
export GSL_ROOT_DIR="/ccc/products/gsl-2.1/intel--19.0.5.281/default"
export PKG_CONFIG_PATH=${GSL_ROOT_DIR}/lib/pkgconfig/:${PKG_CONFIG_PATH}

